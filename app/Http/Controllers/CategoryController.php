<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function getAll()
    {
        try {
            $categories = Category::all();

            return response()->json([
                "code" => 200,
                "message" => "success get categories",
                "data" => $categories
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function getById($id)
    {
        try {
            $category = Category::find($id);

            if (!$category) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {
                return response()->json([
                    "code" => 200,
                    "message" => "success get category by id",
                    "data" => $category
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function add(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "code" => 400,
                    "message" => "bad request",
                    "error" => $validator->errors()
                ], 400);
            } else {

                $inserted = Category::create($validator->valid());

                return response()->json([
                    "code" => 201,
                    "message" => "success insert data",
                    "data" => $inserted
                ], 201);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $category = Category::find($id);

            if (!$category) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "code" => 400,
                        "message" => "bad request",
                        "error" => $validator->errors()
                    ], 400);
                } else {

                    $updated = $category::where('id', $id)->update($validator->valid());

                    return response()->json([
                        "code" => 200,
                        "message" => "success update data",
                        "data" => $updated
                    ]);
                }
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function delete($id)
    {
        try {
            $category = Category::find($id);

            if (!$category) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {

                $category->delete();

                return response()->json([
                    "code" => 200,
                    "message" => "success delete data",
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }
}
