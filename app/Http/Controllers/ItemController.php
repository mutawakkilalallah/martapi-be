<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    public function getAll()
    {
        try {
            $items = Item::filter(request(['cari', 'brand', 'category', 'order']))->paginate(request('limit'));

            return response()->json([
                "code" => 200,
                "message" => "success get items",
                "data" => $items
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function getById($id)
    {
        try {
            $item = Item::find($id);

            if (!$item) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {
                return response()->json([
                    "code" => 200,
                    "message" => "success get item by id",
                    "data" => $item
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function add(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'brand_id' => 'required',
                'category_id' => 'required',
                'number' => 'required',
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "code" => 400,
                    "message" => "bad request",
                    "error" => $validator->errors()
                ], 400);
            } else {

                $inserted = Item::create($validator->valid());

                return response()->json([
                    "code" => 201,
                    "message" => "success insert data",
                    "data" => $inserted
                ], 201);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $item = Item::find($id);

            if (!$item) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {
                $validator = Validator::make($request->all(), [
                    'brand_id' => 'required',
                    'category_id' => 'required',
                    'number' => 'required',
                    'name' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "code" => 400,
                        "message" => "bad request",
                        "error" => $validator->errors()
                    ], 400);
                } else {

                    $updated = $item::where('id', $id)->update($validator->valid());

                    return response()->json([
                        "code" => 200,
                        "message" => "success update data",
                        "data" => $updated
                    ]);
                }
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function delete($id)
    {
        try {
            $item = Item::find($id);

            if (!$item) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {

                $item->delete();

                return response()->json([
                    "code" => 200,
                    "message" => "success delete data",
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }
}
