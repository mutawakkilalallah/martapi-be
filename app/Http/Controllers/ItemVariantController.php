<?php

namespace App\Http\Controllers;

use App\Models\ItemVariant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItemVariantController extends Controller
{
    public function add(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'item_id' => 'required',
                'code' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "code" => 400,
                    "message" => "bad request",
                    "error" => $validator->errors()
                ], 400);
            } else {

                $inserted = ItemVariant::create($validator->valid());

                return response()->json([
                    "code" => 201,
                    "message" => "success insert data",
                    "data" => $inserted
                ], 201);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $itemVariant = ItemVariant::find($id);

            if (!$itemVariant) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {
                $validator = Validator::make($request->all(), [
                    'item_id' => 'required',
                    'code' => 'required',
                    'description' => 'required',
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        "code" => 400,
                        "message" => "bad request",
                        "error" => $validator->errors()
                    ], 400);
                } else {

                    $updated = $itemVariant::where('id', $id)->update($validator->valid());

                    return response()->json([
                        "code" => 200,
                        "message" => "success update data",
                        "data" => $updated
                    ]);
                }
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }

    public function delete($id)
    {
        try {
            $itemVariant = ItemVariant::find($id);

            if (!$itemVariant) {
                return response()->json([
                    "code" => 404,
                    "message" => "not found",
                    "error" => "data not found"
                ], 404);
            } else {

                $itemVariant->delete();

                return response()->json([
                    "code" => 200,
                    "message" => "success delete data",
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                "code" => 500,
                "message" => "internal server error",
                "error" => $th
            ], 500);
        }
    }
}
