<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [''];

    protected $with = ['brand', 'category', 'itemVariants'];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['cari'] ?? false, function ($query, $cari) {
            return $query->where('name', 'like', '%' . $cari . '%');
        });

        $query->when($filters['brand'] ?? false, function ($query, $brand) {
            return $query->where('brand_id', $brand);
        });

        $query->when($filters['category'] ?? false, function ($query, $category) {
            return $query->where('category_id', $category);
        });

        $query->when($filters['order'] ?? false, function ($query, $order) {
            return $query->orderBy('updated_at', $order);
        });
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function itemVariants()
    {
        return $this->hasMany(ItemVariant::class);
    }
}
