<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemVariant;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Brand::create([
            "id" => 1,
            "name" => "Asus"
        ]);

        Category::create([
            "id" => 1,
            "name" => "ROG"
        ]);
        Category::create([
            "id" => 2,
            "name" => "TUF"
        ]);

        Item::create([
            "id" => 1,
            "brand_id" => 1,
            "category_id" => 1,
            "number" => 001,
            "name" => "Laptop Asus ROG Strix"
        ]);

        Item::create([
            "id" => 2,
            "brand_id" => 1,
            "category_id" => 2,
            "number" => 001,
            "name" => "Laptop Asus TUF Dash"
        ]);

        ItemVariant::create([
            "id" => 1,
            "item_id" => 1,
            "code" => "32-2048",
            "description" => "Ram 32 GB / SSD 2 TB"
        ]);

        ItemVariant::create([
            "id" => 2,
            "item_id" => 1,
            "code" => "16-1024",
            "description" => "Ram 16 GB / SSD 1 TB"
        ]);

        ItemVariant::create([
            "id" => 3,
            "item_id" => 2,
            "code" => "16-1024",
            "description" => "Ram 16 GB / SSD 1 TB"
        ]);

        ItemVariant::create([
            "id" => 4,
            "item_id" => 2,
            "code" => "8-512",
            "description" => "Ram 16 GB / SSD 512 GB"
        ]);
    }
}
