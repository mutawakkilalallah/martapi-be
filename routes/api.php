<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemVariantController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/item')->group(function () {
    Route::get('/', [ItemController::class, 'getAll']);
    Route::get('/{id}', [ItemController::class, 'getById']);
    Route::post('/', [ItemController::class, 'add']);
    Route::put('/{id}', [ItemController::class, 'update']);
    Route::delete('/{id}', [ItemController::class, 'delete']);
});

Route::prefix('/item-variant')->group(function () {
    Route::post('/', [ItemVariantController::class, 'add']);
    Route::put('/{id}', [ItemVariantController::class, 'update']);
    Route::delete('/{id}', [ItemVariantController::class, 'delete']);
});

Route::prefix('/category')->group(function () {
    Route::get('/', [CategoryController::class, 'getAll']);
    Route::get('/{id}', [CategoryController::class, 'getById']);
    Route::post('/', [CategoryController::class, 'add']);
    Route::put('/{id}', [CategoryController::class, 'update']);
    Route::delete('/{id}', [CategoryController::class, 'delete']);
});

Route::prefix('/brand')->group(function () {
    Route::get('/', [BrandController::class, 'getAll']);
    Route::get('/{id}', [BrandController::class, 'getById']);
    Route::post('/', [BrandController::class, 'add']);
    Route::put('/{id}', [BrandController::class, 'update']);
    Route::delete('/{id}', [BrandController::class, 'delete']);
});
